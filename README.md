# gitlab-ci-patterns

How to write idiomatic gitlab-ci.yml? Is there something like that?

This project aims to grow into a set of reusable patterns for your GitLab CI
configuration. Feel free to copy&paste or use
[include](https://docs.gitlab.com/ee/ci/yaml/#includefile). If including, I'd
suggest pinning to a specific SHA.

## Environment variables:

`ROTTER_IO_GITLAB_CI_PATTERNS` - provides a set of bash functions useful in the GitLab CI jobs. Please run:

```eval "$ROTTER_IO_GITLAB_CI_PATTERNS"```

to make them available in your job script.

Currently the provided functions are:
- [Collapsible section](https://docs.gitlab.com/ee/ci/jobs/#custom-collapsible-sections) helpers (please see the `collapsible-sections-demo` job in the [.gitlab-ci.yml](.gitlab-ci.yml)):
  - `gitlab_section_start section_name "header text"`
  - `gitlab_section_end section_name`
  - `gitlab_collapsed_section_start section_name "header text`

## Rules
These are to be used in the [rules](https://docs.gitlab.com/ee/ci/yaml/#rules) section of the job and replace the not-always readable explicit variable comparisons.

You can use [!reference](https://docs.gitlab.com/ee/ci/yaml/yaml_optimization.html#reference-tags) custom tag to use them in your jobs like here:

```
  rules:
      - if: !reference [.rotter.io/gitlab-ci-patterns/rules, only_default_branch]
```

## Patterns

- [(Example)](examples/needs_instant_run.yml) - Use `needs: []` to launch jobs from various stages as soon as the pipeline is created
